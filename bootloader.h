/*
Copyright (c) 2022 by Rainer Wahler, RWahler@gmx.net

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Dieses Programm ist Freie Software: Sie k�nnen es unter den Bedingungen
der GNU General Public License, wie von der Free Software Foundation,
Version 3 der Lizenz oder jeder neueren ver�ffentlichten Version,
weiter verteilen und/oder modifizieren.

Dieses Programm wird in der Hoffnung bereitgestellt, dass es n�tzlich sein wird, jedoch
OHNE JEDE GEW�HR,; sogar ohne die implizite
Gew�hr der MARKTF�HIGKEIT oder EIGNUNG F�R EINEN BESTIMMTEN ZWECK.
Siehe die GNU General Public License f�r weitere Einzelheiten.

Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
*/

#ifndef BOOTLOADER_H
	#define	BOOTLOADER_H

	#include "commonHelpers.h"
	#include <avr/io.h>
	#include <util/delay.h>

	#define BAUD_RATE 115200
	#define USART_BAUD_RATE() ((float)(F_CPU * 64 / (16 * (float)BAUD_RATE)) + 0.5)

	// Bootloader Version
	#define VERSION 4
	// Wartezeit (100ms Schritte) ob der Bootloader angefordert wurde
	#define REQUEST_TIMEOUT 30
	#define REQUEST_CHARACTER '?'

	// Attiny 160x Serie
	#define SERIAL_PORT PORTB
	#define SERIAL_TXD PIN2_bm
	#define SERIAL_RXD PIN3_bm

	#define SERIAL_CTRLB USART0.CTRLB
	#define SERIAL_BAUD USART0.BAUD
	#define SERIAL_STATUS USART0.STATUS
	#define SERIAL_RX_DATA USART0.RXDATAL
	#define SERIAL_TX_DATA USART0.TXDATAL

	#define BOOTLOADER_REQUESTED 0xEB
	#define FIRMWARE_IDENTIFIER 0xAC

	// BOOTEND_FUSE * 256 muss die n�chste Gr��e oberhalb des Bootloader Programmcodes betragen
	#define BOOTEND_FUSE               (0x02)
	#define BOOT_SIZE                  (BOOTEND_FUSE * 0x100)
	#define MAPPED_APPLICATION_START   (MAPPED_PROGMEM_START + BOOT_SIZE)
	#define MAPPED_APPLICATION_SIZE    (MAPPED_PROGMEM_SIZE - BOOT_SIZE)

#endif	/* BOOTLOADER_H */

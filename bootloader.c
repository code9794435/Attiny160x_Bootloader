/*
Copyright (c) 2022 by Rainer Wahler, RWahler@gmx.net
*/

/******************************************************************************
 * Die nachfolgenden Einstellungen gehen davon aus, dass der Bootloader nicht gr��er als 512 Bytes ist
 * Einstellungen f�r das Bootloader Projekt
 *		Conf->Avr GCC->Linker->General:
 *			"Do not use standard start files" muss gesetzt sein
 *
 *		Conf->PICkit 4->Memories to program:
 *			"Auto select memories and ranges" auf manuell setzen
 *			Alle Speicher-Typen anhaken
 *			"Program Memory Ranges(s)(hex)" auf "0-FF" setzen (solange der Bootloader nicht gr��er als 512 Bytes ist)
 *
 *		Conf->PICkit 4->Program options:
 *			"Erase all before program" muss gesetzt sein
 *			Es wird der gesamte Flash/EEProm/ID Speicher gel�scht, inklusive der bestehenden Firmware!
 *
 * Einstellungen in bootloader.h
 *		Zum Controller passendes #define MCU_TYPE_... einkommentieren
 *		BAUD_RATE anpassen
 *		BOOTEND_FUSE anpassen. BOOTEND_FUSE * 256 muss die n�chste Gr��e oberhalb des Bootloader Programmcodes betragen
 *
 *
 * Einstellungen f�r das Ziel-Firmware Projekt
 *		Conf->Avr GCC->avr-ld->Memory settings:
 *			"FLASH segment" Eintrag ".text=0x100" erg�nzen. Der Wert betr�gt BOOTEND_FUSE * 256 / 2 (wegen 16Bit Speicherzellen)
 *
 *		Conf->PICkit 4->Memories to program: (die nachfolgenden Einstellungen sind nur notwendig, wenn der Code �ber den PICkit4 geflasht werden soll)
 *			"Auto select memories and ranges" auf manuell setzen
 *			"Configuration Memory","EEPROM","ID" abhaken
 *			"Program Memory" anhaken
 *			"Program Memory Ranges(s)(hex)" auf "0-3FFF" setzen (abh�ngig von der Flash Gr��e des Controllers)
 *			"Preserve Program Memory" anhaken
 *			"Preserve Program Memory Ranges(s)(hex)" auf "0-FF" setzen (solange der Bootloader nicht gr��er als 512 Bytes ist)
 *****************************************************************************/

#include "bootloader.h"

// Fuse Konfiguration
FUSES = {
	.BODCFG = 0x48, // SLEEP=DIS, ACTIVE=SAMPLED, SAMPFREQ=1KHZ, LVL=2.6V
	.OSCCFG = FREQSEL_20MHZ_gc,
	.SYSCFG0 = CRCSRC_NOCRC_gc | RSTPINCFG_UPDI_gc,
	.SYSCFG1 = SUT_1MS_gc,
	.APPEND = 0x00,
	.BOOTEND = BOOTEND_FUSE
};

// Application pointer type
typedef void (*const app_t)(void);

static void v_secureWrite();

static void v_Init() {
	// Bootloader Leseschutz aktivieren
	_PROTECTED_WRITE(NVMCTRL.CTRLB, NVMCTRL_BOOTLOCK_bm);

	// TXD als Ausgang und RXD als Eingang konfigurieren
	SERIAL_PORT.DIRCLR = SERIAL_RXD;
	SERIAL_PORT.DIRSET = SERIAL_TXD;

	// Baudrate setzen
	SERIAL_BAUD = (uint16_t) USART_BAUD_RATE();

	// UART aktivieren
	SERIAL_CTRLB |= USART_RXEN_bm | USART_TXEN_bm;
}

/* Empfangene Daten abfragen */
static uint8_t v_uartReceive() {
	while(!(SERIAL_STATUS & USART_RXCIF_bm));

	return SERIAL_RX_DATA;
}

/* Zeichen senden
 * u8_byte = Zeichen welches gesendet werden soll
 * */
static void v_uartSendChar(uint8_t u8_byte) {
	// TXCIF Flag l�schen
	SERIAL_STATUS |= USART_TXCIF_bm;

	// Zeichen senden
	SERIAL_TX_DATA = u8_byte;

	// Warten bis das Zeichen gesendet wurde
	while (!(SERIAL_STATUS & USART_TXCIF_bm));
}

/* String senden 
 * c_str = String welcher gesendet werden soll
 */
static void v_uartSendString(char *c_str) {
	uint8_t u8_counter = 0;

	while (c_str[u8_counter] != 0)
		v_uartSendChar(c_str[u8_counter++]);
}

/* Schreiben in den Flash oder das EEPROM starten */
static void v_secureWrite() {
	_PROTECTED_WRITE_SPM(NVMCTRL.CTRLA, NVMCTRL_CMD_PAGEERASEWRITE_gc);
}

/* Userrow speichern */
static void v_programUserrow() {
	v_secureWrite();
	while(NVMCTRL.STATUS & NVMCTRL_EEBUSY_bm);
}

/* Kommandozeile anzeigen */
static void v_showCommand() {
	v_uartSendString("\nCmd:\n");
}

/* OK anzeigen */
static void v_showOK(void) {
	v_uartSendString("OK\n");
}

/* Pr�fen ob der Bootloader gestartet werden soll */
static bool b_isBootloaderRequested() {
	uint8_t u8_Counter;

	if (USERROW.USERROW31 == BOOTLOADER_REQUESTED) {
		// Firmware hat den Bootloader angefordert

		USERROW.USERROW31 = 0xFF;
		v_programUserrow();
		return true;
	}

	for (u8_Counter = 0; u8_Counter < REQUEST_TIMEOUT; u8_Counter++) {
		v_uartSendChar(REQUEST_CHARACTER);
		_delay_ms(100);
		if (SERIAL_RX_DATA == 'b')
			return true;
	}

	if (USERROW.USERROW30 == FIRMWARE_IDENTIFIER)
		// Es wurde bereite eine Firmware hochgeladen
		return false;

	// Bootloader wurde zwar nicht angefordert, aber es gibt auch noch keine Firmware die gestartet werden k�nnte
	return true;
}

/* Firmware starten */
void v_startFirmware() {
	v_showOK();

	// Applikationscode Schreibschutz aktivieren
	_PROTECTED_WRITE(NVMCTRL.CTRLB, NVMCTRL_BOOTLOCK_bm | NVMCTRL_APCWP_bm);

	// Starte Applikation, welche direkt nach dem Bootloader liegt
	app_t app = (app_t)(BOOT_SIZE / sizeof(app_t));
	app();
}

/*
 * Haupt Boot-Funktion
 * In den Konstruktor (.ctors) schreiben um Flash zu sparen
 * "naked" Attribut verwendet, weil die Funktion keinen Prolog oder Epilog verwendet
 */
__attribute__((naked)) __attribute__((section(".ctors"))) void boot(void) {
	uint8_t u8_rxData;
	uint16_t u16_flashSize, u16_counter;

	// Initialisiere system f�r AVR GCC Unterst�tzung (erwartet r1 = 0)
	asm volatile("clr r1");

	v_Init();

	_delay_ms(10);
	v_uartSendString("BL");
	v_uartSendChar(0x30 + VERSION);

	// Pr�fen ob der Bootloader gestartet werden soll
	if (!b_isBootloaderRequested())
		v_startFirmware();

	while(1) {
		v_showCommand();
		u8_rxData = v_uartReceive();

		if (u8_rxData == 'v')
			// Firmware Flag setzen
			v_uartSendChar(0x30 + VERSION);
		else if (u8_rxData == '+') {
			// Firmware Flag setzen
			USERROW.USERROW30 = FIRMWARE_IDENTIFIER;
			v_programUserrow();
			v_showOK();
		}
		else if (u8_rxData == '-') {
			// Firmware Flag l�schen
			USERROW.USERROW30 = 0xFF;
			v_programUserrow();
			v_showOK();
		}
		else if (u8_rxData == 's') {
			// Firmware starten
			v_startFirmware();
		}
		else if (u8_rxData == 'r') {
			// Reset durchf�hren
			_PROTECTED_WRITE(RSTCTRL.SWRR, RSTCTRL_SWRE_bm);
		}
		else if (u8_rxData == 'f') {
			// Firmware flashen
			v_showOK();

			// Gr��e der zu flashenden Daten ermitteln
			u8_rxData = v_uartReceive();
			u16_flashSize = u8_rxData;
			u16_flashSize <<= 8;
			u16_flashSize += v_uartReceive();

			// Gr��e zur�ck schicken
			v_uartSendChar(u8_rxData);
			v_uartSendChar((uint8_t)(u16_flashSize & 0xFF));
			v_uartSendChar('\n');

			// Firmware flashen
			uint8_t *app_ptr = (uint8_t *)MAPPED_APPLICATION_START;
			for (u16_counter = 0; u16_counter < u16_flashSize; u16_counter++) {
				// Byte empfangen
				u8_rxData = v_uartReceive();

				// Erst in den Page Buffer laden bevor es in den Flash geschrieben wird
				*app_ptr = u8_rxData;
				app_ptr++;
				if (!((uint16_t)app_ptr % MAPPED_PROGMEM_PAGE_SIZE)) {
					// Seitengrenze (64 Bytes) erreicht und in den Flash schreiben
					v_secureWrite();
					while (NVMCTRL.STATUS & NVMCTRL_FBUSY_bm);
				}

				// Byte als Echo zur�ckschicken
				v_uartSendChar(u8_rxData);
			}

			// Am Ende OK ausgeben, damit das Firmware Upload Skript einen erfolgreichen Abschluss erkennen kann
			v_showOK();
		}
		// Nicht mehr genug Speicherplatz um die Hilfe noch anzuzeigen
//		else
//			// Hilfe anzeigen
//			v_uartSendString("(+,-,s,r,f)");
	}
}
